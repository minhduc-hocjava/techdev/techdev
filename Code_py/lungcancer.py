#
# from ucimlrepo import fetch_ucirepo
#
# # fetch dataset
# lung_cancer = fetch_ucirepo(id=62)
#
# #creat dataframe
# import pandas as pd
# df = pd.DataFrame(lung_cancer.data.features)
#
# df['target'] = lung_cancer.data.targets
#
# df #display dataframe
# df = df.dropna()
#
#
# #create dataset from dataframe
# X = df.loc[:, 'Attribute1':'Attribute56']
# y = df.loc[:, 'target']
#
# print('\n(X_data, y_data):\n', X,'\n',y)
# #split on dataset into training 80%
# from sklearn import model_selection
# X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2, random_state=1234)
#
# method = []
# evaluation = []
#
# """#Logistic Regression
#
#
#
# """
#
# #training
# from sklearn import linear_model
# model = linear_model.LogisticRegression()
# model.fit(X_train, y_train)
#
# #evaluation
# from sklearn import metrics
# y_test_hat = model.predict(X_test)
# print(metrics.accuracy_score(y_test, y_test_hat))
# print(metrics.f1_score(y_test, y_test_hat, average = 'macro')) #f1_score ??
#
#
#
# method.append("Logistic Regression")
# evaluation.append(metrics.f1_score(y_test, y_test_hat, average = 'macro'))
#
# """#Support vector machine"""
#
# #training
# from sklearn import svm
# model =svm.SVC(kernel = "linear", C=100)
# model.fit(X_train, y_train)
#
# #evaluation
# from sklearn import metrics
# y_test_hat = model.predict(X_test)
# print(metrics.accuracy_score(y_test, y_test_hat))
# print(metrics.f1_score(y_test, y_test_hat, average = 'macro')) #f1_score ??
#
#
# #visualization
# import matplotlib.pyplot as plt
# y_test, y_test_hat
# metrics.ConfusionMatrixDisplay.from_predictions(y_test, y_test_hat,labels = [1,2,3], colorbar = False)
# plt.xlabel("Predicted")
# plt.ylabel("Actual")
# plt.title ("Validation set")
# plt.show()
#
#
# method.append("Support vector machine")
# evaluation.append(metrics.f1_score(y_test, y_test_hat, average = 'macro'))
#
# modelSVM = model
#
# """#k-Nearest neighbors"""
#
# #training
# from sklearn import neighbors
# model = neighbors.KNeighborsClassifier(n_neighbors= 3, p = 2) # p = 1 : Manhattan, p = 2: Euclidean
# model.fit(X_train, y_train)
#
# #evaluation
# from sklearn import metrics
# y_test_hat = model.predict(X_test)
# print(metrics.accuracy_score(y_test, y_test_hat))
# print(metrics.f1_score(y_test, y_test_hat, average = 'macro')) #f1_score ??
#
#
# method.append("k-Nearest neighbors")
# evaluation.append(metrics.f1_score(y_test, y_test_hat, average = 'macro'))
#
# """#Gaussian NB"""
#
# #training
# from sklearn import naive_bayes
# model = naive_bayes.GaussianNB()
# model.fit(X_train, y_train)
# #evalation
# from sklearn import metrics
# y_test_hat = model.predict(X_test)
# print(y_test_hat)
# print(metrics.accuracy_score(y_test, y_test_hat))
# print(metrics.f1_score(y_test, y_test_hat, average = 'macro'))
# print(model.predict_proba(X_test))
#
#
# method.append("Gaussian NB")
# evaluation.append(metrics.f1_score(y_test, y_test_hat, average = 'macro'))
#
# """#Decision tree
#
# """
#
# #trainning
# from sklearn import tree
# model = tree.DecisionTreeClassifier(random_state=0)
# model.fit(X_train, y_train)
# #evaluation
# from sklearn import metrics
# y_test_hat = model.predict(X_test)
# print(y_test_hat)
# print(metrics.accuracy_score(y_test, y_test_hat))
# print (metrics.f1_score(y_test, y_test_hat, average = 'macro'))
#
#
# method.append("Decision tree")
# evaluation.append(metrics.f1_score(y_test, y_test_hat, average = 'macro'))
#
# modelDTree = model
#
# """#Neural network
#
#
# """
#
# # normalize X_data
# from tensorflow.keras import layers
# norm_layer = layers.Normalization(axis=-1) # preprocessing layer which normalizes cont. features
# norm_layer.adapt(X) # learns mean, variance
# Xn = norm_layer(X)
# Xn_test = norm_layer(X_test)
#
# # training
# from tensorflow.keras import models, layers, losses, optimizers
# model = models.Sequential ([
#     layers.Dense(units=3, activation='relu', name='layer1'),
#     layers.Dense(units=1, activation='sigmoid', name='layer2')
# ])
# model.compile(
#     loss=losses.BinaryCrossentropy(),
#     optimizer=optimizers.Adam(learning_rate=0.01)
# )
# model.fit(Xn, y, epochs=1000, verbose=0)
#
# # evaludation
# from sklearn import metrics
# predictions = model.predict(Xn_test)
# Y_test_hat = (predictions >= 0.5).astype(int)
# print(metrics.accuracy_score(y_test, Y_test_hat))
# print (metrics.f1_score(y_test, y_test_hat, average = 'macro'))
#
#
#
# """#Dataframe
#
# """
#
# import pandas as pd
# import numpy as np
# from sklearn.metrics import r2_score
#
# # data = {
# #     'Method': ["Linear regression", "Polynomial regression (degree = 2)", "Polynomial regression (degree = 3)", "Neural network" ],
# #     'Validation': [0.5363864868822312,0.4306383406798008,-23916.367510281623,0.42567247407710707],
# #     'Evaluation': [0.46166155590805835,0.37071411807427745,-17547.578518727114,0.39424299711260313]
# #     }
#
#
# data = {
#  'Method': method,
#  'F1': evaluation
# }
# index= []
# count = 0;
#
# for i in data["Method"]:
#   count  = count + 1
#   index.append(count)
#
# df = pd.DataFrame(data, index)
# df
#
# # !pip install m2cgen
# # import m2cgen as m2c
# # print (m2c.export_to_python(modelSVM))
# #
