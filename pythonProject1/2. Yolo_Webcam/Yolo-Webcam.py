from ultralytics import YOLO
import cv2
import cvzone
import  math

#  nếu có cài đặt camera ngoài thì ta set id vào videoCapture nếu chỉ có 1 thì = 0
cap = cv2.VideoCapture(0) # For webcam
# chỉnh khung webcam 3, 4 là Id number
cap.set(3, 1250)
cap.set(4, 720)

# cap = cv2.VideoCapture("../Videos/...") # For video

model = YOLO("../Yolo-Weights/yolov8n.pt")

classNames = ["person", "bicycle", "car","motorbike", "aeroplance", "bus", "truck", "boat"]
while True:
    success, img = cap.read()
    results = model (img, stream= True)
    for r in results:
        # tại sao r chấm được boxes
        boxes = r.boxes
        for box in boxes:
            # xyxy = xyweighthigh
            # box xyxy nó là một gói cần lấy element đầu

            #---------BoundingBox
            # CV2
            x1, y1, x2, y2 = box.xyxy[0]
            x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)
            # print(x1, y1, x2, y2)
            # cv2.rectangle(img,(x1, y1), (x2, y2), (255,255,0), 2)

            # Cvzone
            w, h = x2 - x1, y2 - y1
            cvzone.cornerRect(img, (x1, y1, w, h)) # màu, độ dày của box cvzone tự mặc định

            #------------Confidence
            conf = math.ceil((box.conf[0] *100))/100
            #math.ceil((box.conf[0] *100))/100 to percent
            print(conf)

            # x1, y1 là yêu cầu độ cao nó sát khung (littel bit) muốn nó đẩy lên thì trừ 20 or 30 or 50
            # x1, y1 - 20 nêu để như này nó được đẩy lên tách khỏi khung nhưng nó sẽ bị mất nếu hình ảnh di chuyển
            #  ra khỏi khung vì thế sẽ dùng max (0, x) max (35, y)
            cvzone.putTextRect(img,f'{conf}',(max (0, x1), max (35, y1)))
            #khi thêm text vào rectangle cv2 sẽ không tự canh vào giữa và top inside rectangle còn cvzone thì được

            #----------Class Name
            cls = box.cls[0]
            cvzone.putTextRect(img, f'{cls}{conf}', (max(0, x1), max(35, y1)), scale=1, thickness=1)
            # cls = int(box.cls[0])
            # cvzone.putTextRect(img, f'{classNames[cls]}{conf}', (max(0, x1), max(35, y1)), scale=1, thickness=1)





    cv2.imshow("Image", img)
    cv2.waitKey(1)