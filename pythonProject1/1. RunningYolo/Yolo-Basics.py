from ultralytics import YOLO
import cv2

import os
# tải model về lưu trong folder
# n -> nano
# l -> larg
# model = YOLO ('../Yolo-Weights/yolov8l.pt')
# # đưa hình ảnh vào model để detect
# results = model("Images/1.png", show=True)

# imgs = os.listdir("C:/Users/nnpdo/PycharmProjects/pythonProject1/1. RunningYolo/Images")
#
#
# for i in imgs:
#     print(str(i))
#
# print("Image you need: ")
# ch = int(input())

model = YOLO ('../Yolo-Weights/yolov8l.pt')
results = model(cv2.imread("../1. RunningYolo/Images/1.png"), show=True)

print(len(results[0].boxes))
for i in results[0]:
    box = i.boxes
    print("Object type:", box.cls)
    x = box.xyxy
    a = x[0][1]
    print("Coordinates:", box.xyxy)
    print("Probability:", str(box.conf))

cv2.waitKey(0)