from PIL import Image

from PIL import Image, ImageEnhance
img = Image.open("../1. RunningYolo/Images/1.png")
enhancer = ImageEnhance.Brightness(img)
# Lighter
new_img = enhancer.enhance(1.8)
# Darker
# new_img = enhancer.enhance(0.8)
new_img.show()
def truncate(value):
    if (value < 0):
        return 0
    if (value > 255):
        return 255
    return value

if __name__ == "__main__":
    # img = Image.ope n('../1. RunningYolo/Images/1.png')
    pixels = img.load()

    img_new = Image.new(img.mode, img.size)
    pixels_new = img_new.load()
    brightness = 20
    for i in range(img_new.size[0]):
        for j in range(img_new.size[1]):
            r, b, g = pixels[i,j]
            _r = truncate(r + brightness)
            _b = truncate(b + brightness)
            _g = truncate(g + brightness)
            pixels_new[i,j] = (_r, _b, _g, 255)
    img_new.show()