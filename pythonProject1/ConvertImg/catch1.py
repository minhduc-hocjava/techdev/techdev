import cv2
import numpy as np


def process_image(input_path, output_path):
    # Đọc ảnh từ đường dẫn
    image = cv2.imread(input_path)

    # Chuyển đổi ảnh sang không gian màu GRAY
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Tạo mặt cắt dựa trên ngưỡng (threshold)
    _, thresholded = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)

    # Invert mặt cắt để chọn nền đen
    inverted = cv2.bitwise_not(thresholded)

    # Áp dụng mặt cắt vào ảnh gốc để cắt ảnh từ nền đen
    result = cv2.bitwise_and(image, image, mask=inverted)

    cv2.imwrite(output_path, result)

def replace_background(image_path, new_background_path, output_path):
    # Read the image without an alpha channel
    image = cv2.imread(image_path)

    # Read the new background image
    new_background = cv2.imread(new_background_path)

    # Resize the new background image to match the size of the original image
    new_background = cv2.resize(new_background, (image.shape[1], image.shape[0]))

    # Create a mask for the non-black pixels in the original image
    mask = np.all(image[:, :, :3] != [0, 0, 0], axis=-1)

    # Replace the non-black pixels in the original image with the corresponding pixels from the new background
    new_background[mask] = image[mask]

    # Save the resulting image
    cv2.imwrite(output_path, new_background)


def increase_sharpness(image_path, output_path, sharpening_factor=1.5):
    # Đọc ảnh từ đường dẫn
    image = cv2.imread(image_path)

    # Tạo một kernel sharpening
    kernel = np.array([[-1, -1, -1],
                       [-1,  9, -1],
                       [-1, -1, -1]])

    # Áp dụng kernel sharpening để tăng độ rõ nét
    sharpened_image = cv2.filter2D(image, -1, kernel)

    # Lưu ảnh sau khi tăng độ rõ nét
    cv2.imwrite(output_path, sharpened_image)

if __name__ == "__main__":
    input_path = "../1. RunningYolo/Images/1.png"
    output = "../1. RunningYolo/Images/output.png"
    process_image(input_path, output)

    output_ = "../1. RunningYolo/Images/increase.png"
    increase_sharpness(input_path, output_)

    img_background = "../1. RunningYolo/Images/gachloang.jpg"
    output_path = "duong_dan_anh_output.png"

    replace_background(output,  img_background, output_path)
    # cv2.imshow('Result', blended_image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()



