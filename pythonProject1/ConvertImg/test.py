import cv2
import numpy as np

# Đọc ảnh từ file
image = cv2.imread('../../pythonProject1/1. RunningYolo/Images/1.png')

# Chuyển đổi ảnh sang ảnh đen trắng để dễ dàng xử lý
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Áp dụng bộ lọc Canny để phát hiện biên
edges = cv2.Canny(gray, 50, 150)

# Tìm contours trong ảnh
contours, hierarchy = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Tạo một layout chứa các vật thể được tô viền
layout = np.zeros_like(image)

# Tạo một mask có màu trắng (255) ở vùng contours và giữ nguyên màu sắc
cv2.drawContours(layout, contours, -1, (255, 255, 255), thickness=cv2.FILLED)

# Đọc ảnh mới để áp dụng layout
new_image = cv2.imread('../../pythonProject1/1. RunningYolo/Images/gachloang.jpg')

# Resize ảnh mới để có kích thước giống với layout
new_image_resized = cv2.resize(new_image, (layout.shape[1], layout.shape[0]))

# Tạo một mask từ ảnh mới để giữ nguyên màu sắc của vật
new_mask = np.zeros_like(new_image_resized)
cv2.drawContours(new_mask, contours, -1, (255, 255, 255), thickness=cv2.FILLED)

# Áp dụng layout lên ảnh mới với giữ nguyên màu sắc
result = cv2.bitwise_and(new_image_resized, new_mask) + cv2.bitwise_and(layout, layout)

# Hiển thị ảnh kết quả
cv2.imshow('Result Image', result)
cv2.waitKey(0)
cv2.destroyAllWindows()
