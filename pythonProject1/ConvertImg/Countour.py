
import numpy as np
import cv2
from ultralytics import YOLO

img_path = "../1. RunningYolo/Images/1.png"
bg_path = "../1. RunningYolo/Images/xam.jpg"

# Reading image
font = cv2.FONT_HERSHEY_COMPLEX
img_input = cv2.imread(img_path, cv2.IMREAD_COLOR)

# (h, w, d) = img_input.shape
# print("width={}, height={}, depth={}".format(w, h, d))

# Reading same image in another
# variable and converting to gray scale.
img_gray = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)

img_background = cv2.imread(bg_path)
img_background = cv2.resize(img_background, (img_input.shape[1], img_input.shape[0]), interpolation = cv2.INTER_LINEAR)
# Converting image to a binary image
# ( black and white only image).
_, threshold = cv2.threshold(img_gray, 110, 255, cv2.THRESH_BINARY)

# Detecting contours in image.
contours, _= cv2.findContours(threshold, cv2.RETR_TREE,
                            cv2.CHAIN_APPROX_SIMPLE)

# Create a black mask with the same shape as the image
mask = np.zeros_like(img_input, dtype=np.uint8)
check = False
for cnt in contours:
    if (check == False):
        check = True
        continue
    approx = cv2.approxPolyDP(cnt, 0.0009 * cv2.arcLength(cnt, True), True)
    polygon_points = np.array(approx)
    # cv2.polylines(img_input, [polygon_points], isClosed=True, color=(255, 0, 0), thickness=2)

    # Fill the mask with white (255) inside the polygon
    cv2.fillPoly(mask, [polygon_points], color=(255, 255, 255))

overlay = cv2.bitwise_and(img_input, mask)

mask = np.all(overlay[:, :, :3] != [0, 0, 0], axis=-1)

# Replace the non-black pixels in the original image with the corresponding pixels from the new background
img_background[mask] = overlay[mask]
cv2.imwrite("../1. RunningYolo/Images/output.png", img_background)
cv2.imshow('K', img_background)

# Exiting the window if 'q' is pressed on the keyboard.
if cv2.waitKey(0) & 0xFF == ord('q'):
    cv2.destroyAllWindows()