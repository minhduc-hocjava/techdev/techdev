import cv2
import numpy as np

# Đọc ảnh từ file
image = cv2.imread('../../pythonProject1/1. RunningYolo/Images/1.png')
new_color_image = cv2.imread('../../pythonProject1/1. RunningYolo/Images/xam.jpg')

# Chuyển đổi ảnh sang không gian màu HSV
hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

# Đặt giới hạn màu sắc của bức tường trong không gian màu HSV
lower_wall_color = np.array([20, 0, 0])  # Giới hạn dưới màu sắc của bức tường
upper_wall_color = np.array([40, 255, 255])  # Giới hạn trên màu sắc của bức tường

# Tạo mask cho bức tường
wall_mask = cv2.inRange(hsv_image, lower_wall_color, upper_wall_color)

# Dilation và Erosion để cải thiện mask của bức tường
eroded_mask = cv2.erode(wall_mask, None, iterations=2)
dilated_mask = cv2.dilate(eroded_mask, None, iterations=2)

# Resize ảnh mới để có kích thước phù hợp với mask của bức tường
new_color_wall_resized = cv2.resize(new_color_image, (image.shape[1], image.shape[0]))

# Kết hợp màu sắc một cách mượt mà
blended_color_wall = cv2.addWeighted(new_color_wall_resized, 0.7, image, 0.3, 0)

# Tạo mặt nạ chứa màu sắc mới của bức tường
new_wall_mask = cv2.bitwise_and(dilated_mask, dilated_mask, mask=wall_mask)

# Áp dụng màu sắc mới lên toàn bộ bức tường trong ảnh gốc
image[new_wall_mask > 0] = blended_color_wall[new_wall_mask > 0]

# Hiển thị ảnh kết quả
cv2.imshow('Image with Wall Color Changed', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
