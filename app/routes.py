# app/routes.py
import cv2
import numpy as np
import os
import shutil
from dotenv import load_dotenv
from Code_py.lungcancerModel import chiaHai, score
from flask import render_template, jsonify, request, current_app, session
from app import app
from flask_uploads import UploadSet, configure_uploads, IMAGES
photos = UploadSet('photos', IMAGES)
app.config["UPLOADED_PHOTOS_DEST"] = "static/images"  # Thư mục lưu trữ ảnh đã tải lên
configure_uploads(app, photos)

@app.route('/')

@app.route('/home', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        # Retrieve form data
        age_years = int(request.form.get('age_years'))
        sex = int(request.form.get('sex_0male_1female'))
        episode_number = int(request.form.get('episode_number'))
        input = [age_years, sex, episode_number]
        # Example: Call a function to process the data
        result_data = int(score(input))
        if sex == 1:
            if result_data >= 4:

                result_data = 0
            else:
                result_data = 1
        if sex == 0:
            if result_data  <= 3:

                result_data = 0
            else:
                result_data = 1


        # Create a result list to pass to the template
        if sex == 0:
            sex = 'Male'
        else:
            sex = 'Female'
        result = {
            'age_years': age_years,
            'sex': sex,
            'episode_number': episode_number,
            'result_data': result_data
        }
        return render_template('home.html', result=result)
    return render_template('home.html')

def get_image_files():
    # Example: list all files in the "background" folder
    import os
    folder_path = "static/background"
    image_files = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]
    return image_files

@app.route('/about')
def about():
    image_files = get_image_files()
    return render_template('about.html', image_files=image_files)




@app.route('/select_background', methods=['POST'])
def select_image():
    image_files = get_image_files()
    selected_background = None

    if request.method == 'POST':
        selected_background = request.form.get('selected_background')
        # Thực hiện bất kỳ xử lý nào bạn muốn với đường dẫn của background đã chọn

    return render_template('select_background.html', image_files=image_files, selected_background=selected_background)

@app.route('/upload', methods=['POST'])
def upload():

    selected_background_name = request.form.get('selected_background')
    image_files = get_image_files()

    if 'photo' in request.files:
        photo = request.files['photo']
        if photo.filename != '':
            # Lưu ảnh vào thư mục images
            filename = photos.save(photo)
            photo_path = f'/images/{filename}'
            # Đường dẫn đầy đủ của ảnh đã tải lên
            full_path = os.path.join(app.config['UPLOADED_PHOTOS_DEST'], filename)

            return render_template('about.html', image_files=image_files,photo_path=filename,selected_background=selected_background_name)
    return 'Tải lên không thành công'
@app.route('/ghepanh', methods=['POST'])
def ghepanh():

    def process_image(input_path, output_path):
        image = cv2.imread(input_path)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        _, thresholded = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)
        inverted = cv2.bitwise_not(thresholded)
        result = cv2.bitwise_and(image, image, mask=inverted)
        cv2.imwrite(output_path, result)
        return result

    def replace_background(image_path, new_background_path, output_path):
        image = cv2.imread(image_path)
        new_background = cv2.imread(new_background_path)
        new_background = cv2.resize(new_background, (image.shape[1], image.shape[0]))
        mask = np.all(image[:, :, :3] != [0, 0, 0], axis=-1)
        new_background[mask] = image[mask]
        cv2.imwrite(output_path, new_background)

    def increase_sharpness(image_path, output_path, sharpening_factor=1.5):
        image = cv2.imread(image_path)
        kernel = np.array([[-1, -1, -1],
                           [-1, 9, -1],
                           [-1, -1, -1]])

        sharpened_image = cv2.filter2D(image, -1, kernel)
        cv2.imwrite(output_path, sharpened_image)

    selected_background_name = request.form.get('selected_background')
    photo_path = request.form.get('photo_path')

    input_path = f"./static/images/{photo_path}"
    output_path = f"./static/result/result.jpg"
    background_path = f"./static/background/{selected_background_name}"
    process_image(input_path, output_path)
    replace_background(output_path,background_path,output_path)


    image_files = get_image_files()


    return render_template('about.html', result_path=output_path,image_files=image_files, photo_path=photo_path,selected_background=selected_background_name)
    # return f'111111  {input_path} va {background_path}'
