import cv2, os
import numpy as np


def process_image(input_path, output_path):
    image = cv2.imread(input_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, thresholded = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)
    inverted = cv2.bitwise_not(thresholded)
    result = cv2.bitwise_and(image, image, mask=inverted)
    cv2.imwrite(output_path, result)
    return result


def replace_background(image_path, new_background_path, output_path):
    image = cv2.imread(image_path)
    new_background = cv2.imread(new_background_path)
    new_background = cv2.resize(new_background, (image.shape[1], image.shape[0]))
    mask = np.all(image[:, :, :3] != [0, 0, 0], axis=-1)
    new_background[mask] = image[mask]
    cv2.imwrite(output_path, new_background)


def increase_sharpness(image_path, output_path, sharpening_factor=1.5):
    image = cv2.imread(image_path)
    kernel = np.array([[-1, -1, -1],
                       [-1, 9, -1],
                       [-1, -1, -1]])

    sharpened_image = cv2.filter2D(image, -1, kernel)
    cv2.imwrite(output_path, sharpened_image)





input_path = './static/images/1.png'
output_path = './static/background/xam.jpg'




input_path = f"./static/images/1.png"
output_path = f"./static/result/result.jpg"
background_path = f"./static/background/gachloang.jpg"
process_image(input_path, output_path)
replace_background(output_path,background_path,output_path)



